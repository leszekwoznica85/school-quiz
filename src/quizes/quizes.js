const users = angular.module("quizes", ["quizes.service"]);

angular.module("quizes").controller("QuizesPageCtrl", ($scope, QuizesService) => {
  const vm = ($scope.quizesPage = {});
  //console.log('Hello QuizesPageCtrl', QuizesService)
  //const vm = $scope.UsersPageCtrl

  vm.selected = null;
  vm.edit = () => {
    vm.mode = 'edit';
    
  };

  vm.creatNew = () => {
    vm.mode = 'edit';
    vm.formMode = 'new'
    vm.selected = true;
    vm.selectedUserTemp = {}
    console.log(vm.formMode)
  }
  vm.select = (id) => {
    vm.selectedQuiz = QuizesService.getQuizById(id);
    console.log(vm.selectedQuiz)
    vm.selectedQuizTemp = { ...vm.selectedQuiz };
    vm.selected = true;
    vm.mode = 'show';
    vm.formMode = 'edit'
    console.log(vm.formMode)

    //console.log(id)
    //console.log($scope.selected)
  };

  vm.save = (draft) => {
    //console.log(draft);
    if (draft.id) {
      UsersService.updateUser(draft);
    } else {
      draft.id = vm.users.length + 1;
      UsersService.addNewUser(draft);
    }
    vm.refresh();
  };
  vm.refresh = () => {
    QuizesService.fetchQuizes().then((data) => {
      vm.quizes = data;
      //console.log(vm.quizes)
    });
  };
  vm.refresh();
  vm.deleteUser = (id) => {
    //console.log(id)
    UsersService.deleteNewUser(id);
    vm.refresh();
  };
});
