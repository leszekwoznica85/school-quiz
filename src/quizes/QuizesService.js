angular.module("quizes.service", []).service("QuizesService", function ($http) {
    this.updateUser = (draft) => {
      console.log(draft.id);
      return $http.put("http://localhost:3000/users/"+ draft.id,draft)
    };
    this.getQuizById = (id) => {
      return this._quizes.find((u) => u.id == id);
    };
    this.fetchQuizes = () => {
      return $http.get("http://localhost:3000/quiz").then((res) => {
        return (this._quizes = res.data);
      });
    };
    this.addNewUser = (draft) => {
      //console.log(draft);
      return $http.post("http://localhost:3000/users/", draft)
    }
    this.deleteNewUser = (id) => {
      //console.log(draft);
      return $http.delete("http://localhost:3000/users/"+ id,)
    }
  });